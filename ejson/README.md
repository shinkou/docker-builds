# ejson Docker Image
---

This Docker image features a minimal [Shopify/ejson][1] setup. It is based
on [alpine][2] with an image size of just over 10MB.

[1]: https://github.com/Shopify/ejson
[2]: https://hub.docker.com/_/alpine/

## How to Run

First, you can test out if the binary runs by:

```
$ docker run --rm -t shinkou/ejson keygen
```

Or you could move on to directly using it with keys and data safeguarded:

```
$ docker run \
  -v "$HOME/.ejson_keys:/opt/ejson/keys" \
  --rm -t shinkou/ejson \
  keygen -w

$ docker run \
  -v "$HOME/.ejson_keys:/opt/ejson/keys" \
  -v "$HOME/data:/data" \
  --rm -t shinkou/ejson \
  encrypt ./secret.ejson
```
