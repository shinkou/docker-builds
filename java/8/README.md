# OpenJDK Docker Image
---

This Docker image features OpenJDK (JRE + JDK) on [alpine][1].

## How to Run

To compile and run a Java source file and run it:

```
$ docker run -v "$PWD:/data" -w '/data' --entrypoint javac --rm shinkou/java ./Test.java
$ docker run -v "$PWD:/data" -w '/data' --rm shinkou/java Test
Hello world!
```
---
[1]: https://hub.docker.com/_/alpine/
