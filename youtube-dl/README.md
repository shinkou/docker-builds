# youtube-dl Image
---
This Docker image features [youtube-dl][1] installed based on Alpine.

[1]:https://github.com/rg3/youtube-dl
