# ElasticSearch Docker Image
---

This Docker image features ElasticSearch on Debian Stretch.

Please note that at least **262144** of `vm.max_map_count` is needed on the
host machine to enable API access (default port 9300).  On Linux, it can be
set with the following command as root:

```
sysctl -w vm.max_map_count=262144
```

You will also need this line in the configuration file
*/etc/elasticsearch/elasticsearch.yml*:

```
network.host: 0.0.0.0
```
