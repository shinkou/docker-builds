#!/bin/bash
set -e

export USER="$(whoami)"
export GOPATH="$HOME/gocode"

if [ ! -d "$GOPATH/bin" ]; then
	mkdir -p "$GOPATH/bin"
fi

if [ ! -d "$GOPATH/src" ]; then
	mkdir -p "$GOPATH/src"
fi

export PATH="$GOPATH/bin:$PATH"

exec "$@"
