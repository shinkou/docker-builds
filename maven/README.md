# Maven Docker Image
---

This Docker image feature Apache Maven on [alpine][1].

## How to Run

To build a Maven project, run the Docker image like this:

```
$ docker run -v "$PWD:/myrepo" -w '/myrepo' --rm shinkou/maven clean package
```

Or, if you want to reuse the Maven local repository:

```
$ docker run -v "$HOME/.m2:/root/.m2" -v "$PWD:/myrepo" -w '/myrepo' --rm shinkou/maven clean package
```

---
[1]: https://hub.docker.com/_/alpine/
