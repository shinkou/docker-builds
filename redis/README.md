# Redis Docker Image
---

This Docker image features Redis on Debian Stretch.

To accept non-local connections, one way is to disable protected mode.  For
example:

```
docker run --name some-redis -td shinkou/redis --protected-mode no
```
