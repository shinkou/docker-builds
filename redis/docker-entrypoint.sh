#!/bin/bash
set -e

if [ "${1:0:1}" = '-' ] || [ "${1%.conf}" != "$1" ]; then
	set -- redis-server "$@"
fi

exec "$@"
