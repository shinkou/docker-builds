#!/bin/sh
if [ -r /etc/nginx/nginx.conf ]; then
	mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.phpmemcached.bak
fi

chmod +x /docker-entrypoint.sh

wget 'https://github.com/elijaa/phpmemcachedadmin/archive/1.3.0.tar.gz' \
	&& tar -zxvf ./1.3.0.tar.gz -C /opt \
	&& mv /opt/phpmemcachedadmin-1.3.0 /opt/phpmemcachedadmin \
	&& chown -R nginx:nginx /opt/phpmemcachedadmin \
	&& chmod 1777 /opt/phpmemcachedadmin/Config /opt/phpmemcachedadmin/Temp \
	&& rm ./1.3.0.tar.gz

mkdir -p /run/nginx
