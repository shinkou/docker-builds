# PHPMemcachedAdmin Docker Image
---

This Docker image features PHPMemcachedAdmin on [alpine][1].

## How to Run

First, start a container accessible to a local port (e.g. 8080):

```
$ docker run -p 8080:80 shinkou/phpmemcachedadmin
```

Then, open a web browser and point it to
[http://localhost:8080/](http://localhost:8080/) and start using it.

---
[1]: https://hub.docker.com/_/alpine/
