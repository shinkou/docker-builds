# Rust Docker Image
---

This Docker image features Rust installed with [rustup][1] based on Debian
Stretch.

[1]:https://github.com/rust-lang-nursery/rustup.rs
