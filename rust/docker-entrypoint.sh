#!/bin/sh
export USER="$(whoami)"
export PATH="$HOME/.cargo/bin:$PATH"
exec "$@"
