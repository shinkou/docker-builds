# Python Docker Image
---

This Docker image features Python 3 on Alpine.  It contains the following
modules:

- ec2util (mainly for AWS CloudWatch)
- elasticsearch
- pyhive
- pytz
- requests
- sasl
- sqlalchemy
- thrift
- thrift\_sasl
- vertica-python
