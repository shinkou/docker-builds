# Docker Image Build Scripts

### Directory Structure

All build scripts are put under the name of the docker image's main feature,
and its version(s) if necessary.

e.g.

```
jre/8
postgres/10
redis
```

### How To Use

To build the docker image, just change to the docker feature's directory and
issue the build command.

e.g.

```
$ cd ./jre/8
$ docker build -t 'shinkou/jre:8' .
```

Then tag it as "latest" for easier access.

e.g.

```
$ docker tag 'shinkou/jre:8' 'shinkou/jre:latest'
$ docker run --rm -it 'shinkou/jre' java -version
```
