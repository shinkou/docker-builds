#!/bin/bash
set -e

if [ "$1" = 'kafka' ]; then
	if [ -z "$ZOOKEEPER" ]; then
		kafka-server-start.sh /usr/local/lib/kafka/config/server.properties
	else
		kafka-server-start.sh /usr/local/lib/kafka/config/server.properties --override zookeeper.connect="$ZOOKEEPER"
	fi
else
	exec "$@"
fi
