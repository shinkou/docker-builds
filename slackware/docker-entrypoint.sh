#!/bin/bash
export USER="$(whoami)"
export PATH="$HOME/.cargo/bin:$PATH"
exec "$@"
