import org.chasen.mecab.MeCab;
import org.chasen.mecab.Node;
import org.chasen.mecab.Tagger;

public class Test
{
	static
	{
		try
		{
			System.loadLibrary("MeCab");
		}
		catch(UnsatisfiedLinkError e)
		{
			System.err.println
			(
				"Cannot load the example native code.\n"
				+ "Make sure your LD_LIBRARY_PATH contains \'.\'\n"
				+ e
			);

			System.exit(1);
		}
	}

	public static void main(String[] args)
	{
		System.out.println(MeCab.VERSION);
		Tagger tagger = new Tagger();

		for(String arg: args)
		{
			System.out.println(tagger.parse(arg));
/*
			System.out.println(arg);

			Node node = tagger.parseToNode(arg);

			for(;node != null; node = node.getNext())
			{
				System.out.println(node.getSurface() + "\t" + node.getFeature());
			}

			System.out.println ("EOS\n");
*/
		}
	}
}
