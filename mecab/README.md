# MeCab Docker Image

This Docker image demonstrates how the MeCab Java binding is set up.

## How to Run

Simply run the docker image with something like this:

```
$ docker run --rm shinkou/mecab 'メカブの動作確認をしているところです。'
0.996
メカブ  名詞,一般,*,*,*,*,*
の      助詞,連体化,*,*,*,*,の,ノ,ノ
動作    名詞,サ変接続,*,*,*,*,動作,ドウサ,ドーサ
確認    名詞,サ変接続,*,*,*,*,確認,カクニン,カクニン
を      助詞,格助詞,一般,*,*,*,を,ヲ,ヲ
し      動詞,自立,*,*,サ変・スル,連用形,する,シ,シ
て      助詞,接続助詞,*,*,*,*,て,テ,テ
いる    動詞,非自立,*,*,一段,基本形,いる,イル,イル
ところ  名詞,非自立,副詞可能,*,*,*,ところ,トコロ,トコロ
です    助動詞,*,*,*,特殊・デス,基本形,です,デス,デス
。      記号,句点,*,*,*,*,。,。,。
EOS
```
