# kubectl Docker Image
---

This Docker image features a minimal setup of [kubectl][1] on
[Alpine Linux][2] [Docker image][3].

[1]: https://kubernetes.io/docs/tasks/tools/install-kubectl/
[2]: https://alpinelinux.org/
[3]: https://hub.docker.com/_/alpine
