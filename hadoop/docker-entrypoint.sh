#!/bin/bash
set -e

if [ -z "$HDFS_NAMENODE_HOST" ]; then
	HDFS_NAMENODE_HOST=`hostname`
fi

sed "s|\${HDFS_NAMENODE_HOST}|${HDFS_NAMENODE_HOST}|g" $HADOOP_HOME/etc/hadoop/core-site.xml.template > $HADOOP_HOME/etc/hadoop/core-site.xml

if [ -z "$HDFS_NAMENODE_USER" ]; then
	HDFS_NAMENODE_USER=root
fi
if [ -z "$HDFS_DATANODE_USER" ]; then
	HDFS_DATANODE_USER=root
fi
if [ -z "$HDFS_SECONDARYNAMENODE_USER" ]; then
	HDFS_SECONDARYNAMENODE_USER=root
fi
if [ -z "$YARN_RESOURCEMANAGER_USER" ]; then
	YARN_RESOURCEMANAGER_USER=root
fi
if [ -z "$YARN_NODEMANAGER_USER" ]; then
	YARN_NODEMANAGER_USER=root
fi

export HDFS_NAMENODE_USER=$HDFS_NAMENODE_USER
export HDFS_DATANODE_USER=$HDFS_DATANODE_USER
export HDFS_SECONDARYNAMENODE_USER=$HDFS_SECONDARYNAMENODE_USER
export YARN_RESOURCEMANAGER_USER=$YARN_RESOURCEMANAGER_USER
export YARN_NODEMANAGER_USER=$YARN_NODEMANAGER_USER

ENV_SH=$HADOOP_HOME/etc/hadoop/hadoop-env.sh

if [[ -f "$ENV_SH" ]]; then
	rm "$ENV_SH"
fi

echo "export JAVA_HOME=$JAVA_HOME" >> "$ENV_SH"
echo "export HDFS_NAMENODE_USER=$HDFS_NAMENODE_USER" >> "$ENV_SH"
echo "export HDFS_DATANODE_USER=$HDFS_DATANODE_USER" >> "$ENV_SH"
echo "export HDFS_SECONDARYNAMENODE_USER=$HDFS_SECONDARYNAMENODE_USER" >> "$ENV_SH"
echo "export YARN_RESOURCEMANAGER_USER=$YARN_RESOURCEMANAGER_USER" >> "$ENV_SH"
echo "export YARN_NODEMANAGER_USER=$YARN_NODEMANAGER_USER" >> "$ENV_SH"

if [ ! -d ~/.ssh ]; then
	mkdir -p ~/.ssh
fi

if [ ! -f ~/.ssh/id_rsa ]; then
	ssh-keygen -t rsa -q -N "" -f ~/.ssh/id_rsa \
		&& cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys \
		&& chmod 600 ~/.ssh/authorized_keys
fi

service ssh start

if [ 0 -eq $# ]; then
	$HADOOP_HOME/sbin/start-dfs.sh
	tail -f /var/log/lastlog
else
	exec "$@"
fi
