# jq Docker Image
---

This Docker image features a minimal jq setup.  Inspired by
[mwendler/jq][1], this image is also based on [alpine][2]
and sized just over 10MB.

[1]: https://hub.docker.com/r/mwendler/jq
[2]: https://hub.docker.com/_/alpine/

## How to Run

First, you can access the brief help message by:

```
$ docker run --rm shinkou/jq
```

Next, you can pipe in some input:

```
$ curl 'https://api.bitbucket.org/2.0/repositories/shinkou/docker-builds/commits' | docker run -i --rm shinkou/jq -r '.values[] | .hash'
```

Finally, you can create an alias to ease more regular usage:

```
$ alias jq='docker run -i --rm shinkou/jq'
$ curl 'https://api.bitbucket.org/2.0/repositories/shinkou/docker-builds/commits' | jq -r '.values[] | .hash'
```
