#!/bin/bash
set -e

if [ "$1" = 'zookeeper' ]; then
	/usr/share/zookeeper/bin/zkServer.sh start-foreground
else
	exec "$@"
fi
